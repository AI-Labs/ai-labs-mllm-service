FROM registry.gitee-ai.local/base/iluvatar-corex:3.2.0-sd3-bi100

RUN apt-get update && apt-get install -y net-tools ffmpeg libnss3 libpcre3 libpcre3-dev build-essential software-properties-common python3.9 python3-pip git git-lfs curl wget

COPY .ssh /root/.ssh

RUN chmod 600 /root/.ssh/id_rsa

WORKDIR /root/ai-labs

COPY . .

RUN pip install JPype1 ipywidgets jupyterlab jupyterlab-language-pack-zh-CN jupyter-server-proxy pyNetLogo SALib

# RUN pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple

EXPOSE 7860

ENTRYPOINT ["jupyter", "lab", \
            "--allow-root", \
            "--ip=0.0.0.0", \
            "--port=7860", \
            "--no-browser", \
            "--NotebookApp.token=", \
            "--NotebookApp.password=", \
            "--NotebookApp.allow_remote_access=True", \
            "--NotebookApp.allow_origin=*", \
            "--ServerApp.disable_check_xsrf=True"]
