import os

from urllib.parse import urlparse
from torch.hub import download_url_to_file, get_dir

def download_model(model_name):
    from modelscope.hub.snapshot_download import snapshot_download
    os.system(f'rm -rf models/${model_name}')
    snapshot_download(model_name, cache_dir="models/")


def download_easy_wav2lip_model():
    from modules.digital_human.easy_wav2lip.functions import load_model, load_sr, load_predictor
    os.system(f'mkdir -p models/easy_wav2lip')
    download_url_to_file("https://github.com/anothermartz/Easy-Wav2Lip/releases/download/Prerequesits/Wav2Lip_GAN.pth", "models/easy_wav2lip/Wav2Lip_GAN.pth", hash_prefix=None, progress=True)
    download_url_to_file("https://github.com/anothermartz/Easy-Wav2Lip/releases/download/Prerequesits/Wav2Lip.pth", "models/easy_wav2lip/Wav2Lip.pth", hash_prefix=None, progress=True)
    download_url_to_file("https://github.com/anothermartz/Easy-Wav2Lip/releases/download/Prerequesits/GFPGANv1.4.pth", "models/easy_wav2lip/GFPGANv1.4.pth", hash_prefix=None, progress=True)
    download_url_to_file("https://github.com/anothermartz/Easy-Wav2Lip/releases/download/Prerequesits/shape_predictor_68_face_landmarks_GTX.dat", "models/easy_wav2lip/shape_predictor_68_face_landmarks_GTX.dat", hash_prefix=None, progress=True)
    
    model = load_model("models/easy_wav2lip/Wav2Lip_GAN.pth")
    model = load_model("models/easy_wav2lip/Wav2Lip.pth")
    load_sr()
    load_predictor()


if __name__ == '__main__':
    download_model("iic/AnyDoor")
    download_model("iic/SenseVoiceSmall")

    download_model("AI-ModelScope/stable-diffusion-inpainting")
    download_model("AI-ModelScope/stable-diffusion-3-medium-diffusers")
    download_model("Qwen/Qwen2-VL-2B-Instruct")
    download_model("ZhipuAI/glm-4-9b-chat")

    download_easy_wav2lip_model()
