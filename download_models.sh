#!/bin/bash

export HF_ENDPOINT=https://hf-mirror.com

huggingface-cli download --resume-download --local-dir-use-symlinks False GanymedeNil/text2vec-large-chinese --local-dir models/GanymedeNil/text2vec-large-chinese

# chat
huggingface-cli download --resume-download --local-dir-use-symlinks False THUDM/glm-4-9b-chat --local-dir models/ZhipuAI/glm-4-9b-chat
huggingface-cli download --resume-download --local-dir-use-symlinks False 01-ai/Yi-1.5-9B-Chat --local-dir models/01-ai/Yi-1.5-9B-Chat

# voice
huggingface-cli download --resume-download --local-dir-use-symlinks False bert-base-uncased --local-dir models/melotts/bert-base-uncased
huggingface-cli download --resume-download --local-dir-use-symlinks False bert-base-multilingual-uncased --local-dir models/melotts/bert-base-multilingual-uncased

huggingface-cli download --resume-download --local-dir-use-symlinks False 2Noise/ChatTTS --local-dir models/2Noise/ChatTTS
huggingface-cli download --resume-download --local-dir-use-symlinks False fishaudio/fish-speech-1.4 --local-dir models/fishaudio/fish-speech-1.4

huggingface-cli download --resume-download --local-dir-use-symlinks False UsefulSensors/moonshine-base --local-dir models/UsefulSensors/moonshine-base

# image
huggingface-cli download --resume-download --local-dir-use-symlinks False sd-community/sdxl-flash --local-dir models/sd-community/sdxl-flash
huggingface-cli download --resume-download --local-dir-use-symlinks False stabilityai/stable-diffusion-3-medium-diffusers --local-dir models/stabilityai/stable-diffusion-3-medium-diffusers

# vision_language
huggingface-cli download --resume-download --local-dir-use-symlinks False Qwen/Qwen2-VL-2B-Instruct --local-dir models/Qwen/Qwen2-VL-2B-Instruct

# try_on
huggingface-cli download --resume-download --local-dir-use-symlinks False zhengchong/CatVTON --local-dir models/zhengchong/CatVTON
# huggingface-cli download --resume-download --local-dir-use-symlinks False runwayml/stable-diffusion-inpainting --local-dir models/runwayml/stable-diffusion-inpainting

# digital_human
huggingface-cli download --resume-download --local-dir-use-symlinks False fudan-generative-ai/hallo --local-dir models/fudan-generative-ai/hallo
huggingface-cli download --resume-download --local-dir-use-symlinks False fudan-generative-ai/hallo2 --local-dir models/fudan-generative-ai/hallo2

huggingface-cli download --resume-download --local-dir-use-symlinks False tk93/V-Express --local-dir models/tk93/V-Express

# other
huggingface-cli download --resume-download --local-dir-use-symlinks False TMElyralab/MuseTalk --local-dir models/TMElyralab/MuseTalk
huggingface-cli download --resume-download --local-dir-use-symlinks False yzd-v/DWPose --local-dir models/yzd-v/DWPose
huggingface-cli download --resume-download --local-dir-use-symlinks False stabilityai/sd-vae-ft-mse --local-dir models/stabilityai/sd-vae-ft-mse
huggingface-cli download --resume-download --local-dir-use-symlinks False ManyOtherFunctions/face-parse-bisent --local-dir models/face-parse-bisent
