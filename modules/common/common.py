from fastapi import APIRouter, Request, HTTPException, File, UploadFile, Form
from fastapi.responses import StreamingResponse, JSONResponse

import os
import uuid
from datetime import datetime


router = APIRouter(
    prefix='',
    tags = ['通用功能']
)

@router.post("/upload")
async def upload_file(file: UploadFile):
    localdir = f"statics/upload/{datetime.now().strftime('%Y-%m-%d')}/{uuid.uuid4()}"
    os.makedirs(localdir, exist_ok=True)
    localfile = f"{localdir}/{file.filename}"

    with open(localfile, "wb") as f:
        f.write(file.file.read())

    data = { "code": 0, "message": "操作成功", "filepath":  localfile}
    return JSONResponse(status_code=200, content=data)
