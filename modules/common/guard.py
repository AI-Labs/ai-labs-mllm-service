from fastapi import APIRouter, Request, HTTPException, Response
from fastapi.responses import FileResponse

router = APIRouter(
    prefix='',
    tags = ['挡板']
)

@router.post("/generate_image_url")
def generate_image_url(request: dict):
    return "statics/image/cloth.png"


@router.post("/diffusion/generate_image_file")
def generate_image_file(request: dict):
    return FileResponse(generate_image_url(request), media_type="image/jpeg")

