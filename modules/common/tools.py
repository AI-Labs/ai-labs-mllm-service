from fastapi import APIRouter, Request, HTTPException, File, UploadFile, Form
from fastapi.responses import StreamingResponse, JSONResponse

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import re
import os
import uuid
from datetime import datetime


router = APIRouter(
    prefix='/tools',
    tags = ['通用工具']
)


options = webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
options.add_argument('--disable-gpu')
options.add_argument('--remote-debugging-port=9222')
options.add_argument('--single-process')
driver = webdriver.Chrome(options=options)

url_type = 2

if url_type == 1:
    driver.get(url=f"https://m.kuaidi.com/")
elif url_type == 2:
    driver.get(url=f"https://www.ickd.cn/auto.html")


def desensitize_numbers(text):
    pattern = r'1[3-9]\d{9}'
    phone_numbers = re.findall(pattern, text)
    desensitize_phone_numbers = [re.sub(r'(\d{3})\d{6}(\d{2})', r'\1xxxxxx\2', number) for number in phone_numbers]
    zip_phone_numbers = zip(phone_numbers,desensitize_phone_numbers)
    for phone_number in zip_phone_numbers:
        text = text.replace(phone_number[0], phone_number[1])

    return text


def query_express_1(number):
    input_ordernumber = driver.find_element(by=By.NAME, value="ordernumber")
    input_ordernumber.clear()
    input_ordernumber.send_keys(number)

    submit_ordernumber = driver.find_element(by=By.CLASS_NAME, value="js_submit")
    submit_ordernumber.click()

    try:
        result_li = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.CLASS_NAME, "timeline"))
        )
        result_text = result_li.text.replace("点我寄快递，6元就能发全国，更快更省钱！\n", "").replace("\n", "\n\n")
        return desensitize_numbers(result_text)
    except:
        result_li = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.CLASS_NAME, "sorry_bq"))
        )
        result_text = result_li.text.replace("确定", "").replace("\n", "\n\n")
        return result_text


def query_express_2(number):
    input_ordernumber = driver.find_element(by=By.ID, value="mailNo")
    input_ordernumber.clear()
    input_ordernumber.send_keys(number)

    submit_ordernumber = driver.find_element(by=By.ID, value="submitButton")
    submit_ordernumber.click()

    try:
        element = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, "result"))
        )
        return desensitize_numbers(element.text.split("地点和跟踪进度\n")[1].replace("","").replace("\n", "\n\n"))
    except:
        element = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//span[@class='msg']"))
        )
        return element.text


def query_express_3(numbe):
    driver.get(url=f"https://t.17track.net/zh-cn#nums={number}")
    try:
        element = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//dl[@class='trn-block']"))
        )
        return desensitize_numbers(element.text)
    except:
        element = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//div[@class='yqcr-last-event-pc']"))
        )
        return element.text


@router.post("/query_express")
def query_express(number=Form(None)):
    if url_type == 1:
        return query_express_1(number)
    elif url_type == 2:
        return query_express_2(number)
    elif url_type == 3:
        return query_express_3(number)
