import torch
import subprocess
import json
import os
import dlib
import gdown
import pickle
import re

from gfpgan import GFPGANer
from .model import Wav2Lip

from configs import config

device = 'cuda' if torch.cuda.is_available() else 'mps' if torch.backends.mps.is_available() else 'cpu'
model_path = config.service.easy_wav2lip.model_path

def get_video_details(filename):
    cmd = [
        "ffprobe",
        "-v",
        "error",
        "-show_format",
        "-show_streams",
        "-of",
        "json",
        filename,
    ]
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    info = json.loads(result.stdout)
    video_stream = next(
        stream for stream in info["streams"] if stream["codec_type"] == "video"
    )
    width = int(video_stream["width"])
    height = int(video_stream["height"])
    resolution = width * height
    fps = eval(video_stream["avg_frame_rate"])
    length = float(info["format"]["duration"])

    return width, height, fps, length


def format_time(seconds):
    hours = int(seconds // 3600)
    minutes = int((seconds % 3600) // 60)
    seconds = int(seconds % 60)

    if hours > 0:
        return f"{hours}h {minutes}m {seconds}s"
    elif minutes > 0:
        return f"{minutes}m {seconds}s"
    else:
        return f"{seconds}s"


def _load(checkpoint_path):
    if device != "cpu":
        checkpoint = torch.load(checkpoint_path)
    else:
        checkpoint = torch.load(
            checkpoint_path, map_location=lambda storage, loc: storage
        )
    return checkpoint


def load_model(path):
    folder, filename_with_extension = os.path.split(path)
    filename, file_type = os.path.splitext(filename_with_extension)
    results_file = os.path.join(folder, filename + ".pk1")
    if os.path.exists(results_file):
        with open(results_file, "rb") as f:
            return pickle.load(f)

    model = Wav2Lip()
    print("Loading {}".format(path))
    checkpoint = _load(path)
    s = checkpoint["state_dict"]
    new_s = {}
    for k, v in s.items():
        new_s[k.replace("module.", "")] = v
    model.load_state_dict(new_s)

    model = model.to(device)
    with open(results_file, "wb") as f:
        pickle.dump(model.eval(), f)

    return model.eval()


def get_input_length(filename):
    result = subprocess.run(
        [
            "ffprobe",
            "-v",
            "error",
            "-show_entries",
            "format=duration",
            "-of",
            "default=noprint_wrappers=1:nokey=1",
            filename,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )
    return float(result.stdout)


def is_url(string):
    url_regex = re.compile(r"^(https?|ftp)://[^\s/$.?#].[^\s]*$")
    return bool(url_regex.match(string))


def load_predictor():
    checkpoint = os.path.join(
        model_path, "shape_predictor_68_face_landmarks_GTX.dat"
    )
    predictor = dlib.shape_predictor(checkpoint)
    mouth_detector = dlib.get_frontal_face_detector()

    with open(f"{model_path}/predictor.pkl", "wb") as f:
        pickle.dump(predictor, f)

    with open(f"{model_path}/mouth_detector.pkl", "wb") as f:
        pickle.dump(mouth_detector, f)


def load_sr():
    run_params = GFPGANer(
        model_path=f"{model_path}/GFPGANv1.4.pth",
        upscale=1,
        arch="clean",
        channel_multiplier=2,
        bg_upsampler=None,
    )
    return run_params


def upscale(image, properties):
    _, _, output = properties.enhance(
        image, has_aligned=False, only_center_face=False, paste_back=True
    )
    return output
