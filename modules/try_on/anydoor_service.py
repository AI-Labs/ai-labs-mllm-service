import numpy as np
import torch 
import cv2
import random
from PIL import Image

import os
import json
import requests
import einops
import base64
import uuid
from io import BytesIO
from datetime import datetime

from cldm.model import create_model, load_state_dict
from cldm.ddim_hacked import DDIMSampler
from cldm.hack import disable_verbosity

from fastapi import APIRouter, Request, HTTPException, Response, File, UploadFile, Form
from fastapi.responses import FileResponse

from model.cloth_masker import AutoMasker
from diffusers.image_processor import VaeImageProcessor

from configs import config

from omegaconf import OmegaConf

config_path = 'configs/anydoor.yaml'
anydoor_config = OmegaConf.load(config_path)
device = anydoor_config.runtime.device

router = APIRouter(
    prefix='/try_on/anydoor',
    tags = ['虚拟试穿']
)

def gaussian_blure(img, intens = 5):
    """
    高斯模糊
    :param image_path:
    :intens 5,10,15,20
    :return:
    """
    img = np.array(img).astype(np.uint8)
    result = cv2.GaussianBlur(img, (0, 0), intens)
    result = Image.fromarray(result)
    return result

def random_mask(mask):
    h,w = mask.shape[0], mask.shape[1]
    mask_black = np.zeros_like(mask)
    box_w = random.uniform(0.4, 0.9) * w
    box_h = random.uniform(0.4, 0.9) * h
    box_w = int(box_w)
    box_h = int(box_h)
    y1 = random.randint(0, h - box_h)
    y2 = y1 + box_h
    x1 = random.randint(0, w - box_w)
    x2 = x1 + box_w
    mask_black[y1:y2,x1:x2] = 1
    mask_black = mask_black.astype(np.uint8)
    return mask_black

'''
def random_mask_grid(mask, p=0.50):
    # 创建一个 h x w 的全零数组，作为初始掩膜
    h,w = mask.shape[0],mask.shape[1]
    mask = np.zeros((h, w), dtype=np.uint8)
    n = random.choice([3,4,5,6,7,8,9,10])
    
    # 计算小块的大小
    block_h = h // n
    block_w = w // n
    
    # 在每个小块中以概率 p 设置为 1
    for i in range(n):
        for j in range(n):
            if np.random.rand() < p:
                mask[i*block_h:(i+1)*block_h, j*block_w:(j+1)*block_w] = 1
    return mask
'''

def get_SIFT(image):
    orb = cv2.ORB_create(nfeatures=200, edgeThreshold=50)
    keypoint, descriptor = orb.detectAndCompute(image, None)
    coordinates = [(int(kp.pt[1]), int(kp.pt[0])) for kp in keypoint]
    return coordinates


'''
def random_mask_grid(mask, points_list, p=0.0):
    # 创建一个 h x w 的全零数组，作为初始掩膜
    h, w = mask.shape[:2]
    mask = np.zeros((h, w), dtype=np.uint8)
    n = random.choice([3,4,5,6,7,8,9,10])
    
    # 计算小块的大小
    block_h = h // n
    block_w = w // n
    
    # 统计每个小块内的点个数
    block_counts = np.zeros((n, n), dtype=np.int32)
    for point in points_list:
        y, x = point
        i = min(y // block_h, n-1)
        j = min(x // block_w, n-1)
        block_counts[i, j] += 1
    
    # 找出包含点最多的前5个小块
    top5_blocks = np.argpartition(-block_counts.flatten(), 5)[:5]
    
    # 将这些小块对应的像素设为1
    for idx in top5_blocks:
        i, j = divmod(idx, n)
        mask[i*block_h:(i+1)*block_h, j*block_w:(j+1)*block_w] = 1
    
    # 在其他小块中按照概率p设置为1
    for i in range(n):
        for j in range(n):
            if (i*n + j) not in top5_blocks and np.random.rand() < p:
                mask[i*block_h:(i+1)*block_h, j*block_w:(j+1)*block_w] = 1
    
    return mask
'''

def random_mask_grid(mask, points_list, p=0.50, top5_p=0.70, other_p=0.30):
    # 创建一个 h x w 的全零数组，作为初始掩膜
    h, w = mask.shape[:2]
    mask = np.zeros((h, w), dtype=np.uint8)
    n = random.choice([3,4,5,6,7,8,9,10])
    
    # 计算小块的大小
    block_h = h // n
    block_w = w // n
    
    # 统计每个小块内的点个数
    block_counts = np.zeros((n, n), dtype=np.int32)
    for point in points_list:
        y, x = point
        i = min(y // block_h, n-1)
        j = min(x // block_w, n-1)
        block_counts[i, j] += 1
    
    # 找出包含点最多的前5个小块
    top5_blocks = np.argpartition(-block_counts.flatten(), 5)[:5]
    
    # 将这些小块对应的像素设为1
    for idx in top5_blocks:
        i, j = divmod(idx, n)
        if np.random.rand() < top5_p:
            mask[i*block_h:(i+1)*block_h, j*block_w:(j+1)*block_w] = 1
    
    # 在其他小块中按照概率p设置为1
    for i in range(n):
        for j in range(n):
            if (i*n + j) not in top5_blocks and np.random.rand() < other_p:
                mask[i*block_h:(i+1)*block_h, j*block_w:(j+1)*block_w] = 1
    
    return mask

def random_perspective_transform(image, intensity):
    """
    对图像进行随机透视变换

    参数:
    image: 要进行变换的输入图像
    intensity: 变换的强度,范围从0到1,值越大,变换越明显

    返回值:
    变换后的图像
    """
    height, width = image.shape[:2]

    # 生成随机透视变换的四个目标点
    x_offset = width * 0.4 * intensity
    y_offset = height * 0.4 * intensity
    dst_points = np.float32([[random.uniform(-x_offset, x_offset), random.uniform(-y_offset, y_offset)],
                             [width - random.uniform(-x_offset, x_offset), random.uniform(-y_offset, y_offset)],
                             [random.uniform(-x_offset, x_offset), height - random.uniform(-y_offset, y_offset)],
                             [width - random.uniform(-x_offset, x_offset), height - random.uniform(-y_offset, y_offset)]])

    # 对应的源点是图像的四个角
    src_points = np.float32([[0, 0], [width, 0], [0, height], [width, height]])

    # 生成透视变换矩阵
    M = cv2.getPerspectiveTransform(src_points, dst_points)

    # 进行透视变换
    transformed_image = cv2.warpPerspective(image, M, (width, height))
    mask = np.ones_like(transformed_image)
    transformed_mask = cv2.warpPerspective(mask, M, (width, height))> 0.5

    kernel_size = 5
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    transformed_mask = cv2.erode(transformed_mask.astype(np.uint8), kernel, iterations=1).astype(np.uint8)

    white_back = np.ones_like(transformed_image) * 255
    transformed_image = transformed_image * transformed_mask + white_back * (1-transformed_mask)
    return transformed_image




def mask_score(mask):
    '''Scoring the mask according to connectivity.'''
    mask = mask.astype(np.uint8)
    if mask.sum() < 10:
        return 0
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnt_area = [cv2.contourArea(cnt) for cnt in contours]
    conc_score = np.max(cnt_area) / sum(cnt_area)
    return conc_score


def sobel(img, mask, thresh = 50):
    '''Calculating the high-frequency map.'''
    H,W = img.shape[0], img.shape[1]
    img = cv2.resize(img,(256,256))
    mask = (cv2.resize(mask,(256,256)) > 0.5).astype(np.uint8)
    kernel = np.ones((5,5),np.uint8)
    mask = cv2.erode(mask, kernel, iterations = 2)
    
    Ksize = 3
    sobelx = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=Ksize)
    sobely = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=Ksize)
    sobel_X = cv2.convertScaleAbs(sobelx)
    sobel_Y = cv2.convertScaleAbs(sobely)
    scharr = cv2.addWeighted(sobel_X, 0.5, sobel_Y, 0.5, 0)
    scharr = np.max(scharr,-1) * mask    
    
    scharr[scharr < thresh] = 0.0
    scharr = np.stack([scharr,scharr,scharr],-1)
    scharr = (scharr.astype(np.float32)/255 * img.astype(np.float32) ).astype(np.uint8)
    scharr = cv2.resize(scharr,(W,H))
    return scharr


def resize_and_pad(image, box):
    '''Fitting an image to the box region while keeping the aspect ratio.'''
    y1,y2,x1,x2 = box
    H,W = y2-y1, x2-x1
    h,w =  image.shape[0], image.shape[1]
    r_box = W / H 
    r_image = w / h
    if r_box >= r_image:
        h_target = H
        w_target = int(w * H / h) 
        image = cv2.resize(image, (w_target, h_target))

        w1 = (W - w_target) // 2
        w2 = W - w_target - w1
        pad_param = ((0,0),(w1,w2),(0,0))
        image = np.pad(image, pad_param, 'constant', constant_values=255)
    else:
        w_target = W 
        h_target = int(h * W / w)
        image = cv2.resize(image, (w_target, h_target))

        h1 = (H-h_target) // 2 
        h2 = H - h_target - h1
        pad_param =((h1,h2),(0,0),(0,0))
        image = np.pad(image, pad_param, 'constant', constant_values=255)
    return image



def expand_image_mask(image, mask, ratio=1.4, random = False):
    # expand image and mask 
    # pad image with 255 
    # pad mask with 0
    h,w = image.shape[0], image.shape[1]
    H,W = int(h * ratio), int(w * ratio) 
    if random:
        h1 = np.random.randint(0, int(H - h))
        w1 = np.random.randint(0, int(W - w)) 
    else:
        h1 = int((H - h) // 2)
        w1 = int((W -w) // 2)
    h2 = H - h - h1
    w2 = W -w - w1
    pad_param_image = ((h1,h2),(w1,w2),(0,0))
    pad_param_mask = ((h1,h2),(w1,w2))
    image = np.pad(image, pad_param_image, 'constant', constant_values=255)
    mask = np.pad(mask, pad_param_mask, 'constant', constant_values=0)
    return image, mask


def resize_box(yyxx, H,W,h,w):
    y1,y2,x1,x2 = yyxx
    y1,y2 = int(y1/H * h), int(y2/H * h)
    x1,x2 = int(x1/W * w), int(x2/W * w)
    y1,y2 = min(y1,h), min(y2,h)
    x1,x2 = min(x1,w), min(x2,w)
    return (y1,y2,x1,x2)


def get_bbox_from_mask(mask):
    h,w = mask.shape[0],mask.shape[1]

    if mask.sum() < 10:
        return 0,h,0,w
    rows = np.any(mask,axis=1)
    cols = np.any(mask,axis=0)
    y1,y2 = np.where(rows)[0][[0,-1]]
    x1,x2 = np.where(cols)[0][[0,-1]]
    return (y1,y2,x1,x2)


def expand_bbox(mask,yyxx,ratio=[1.2,2.0], min_crop=0):
    y1,y2,x1,x2 = yyxx
    ratio = np.random.randint( ratio[0] * 10,  ratio[1] * 10 ) / 10
    H,W = mask.shape[0], mask.shape[1]
    xc, yc = 0.5 * (x1 + x2), 0.5 * (y1 + y2)
    h = ratio * (y2-y1+1)
    w = ratio * (x2-x1+1)
    h = max(h,min_crop)
    w = max(w,min_crop)

    x1 = int(xc - w * 0.5)
    x2 = int(xc + w * 0.5)
    y1 = int(yc - h * 0.5)
    y2 = int(yc + h * 0.5)

    x1 = max(0,x1)
    x2 = min(W,x2)
    y1 = max(0,y1)
    y2 = min(H,y2)
    return (y1,y2,x1,x2)


def box2squre(image, box):
    H,W = image.shape[0], image.shape[1]
    y1,y2,x1,x2 = box
    cx = (x1 + x2) // 2
    cy = (y1 + y2) // 2
    h,w = y2-y1, x2-x1

    if h >= w:
        x1 = cx - h//2
        x2 = cx + h//2
    else:
        y1 = cy - w//2
        y2 = cy + w//2
    x1 = max(0,x1)
    x2 = min(W,x2)
    y1 = max(0,y1)
    y2 = min(H,y2)
    return (y1,y2,x1,x2)


def pad_to_square(image, pad_value = 255, random = False):
    H,W = image.shape[0], image.shape[1]
    if H == W:
        return image

    padd = abs(H - W)
    if random:
        padd_1 = int(np.random.randint(0,padd))
    else:
        padd_1 = int(padd / 2)
    padd_2 = padd - padd_1

    if H > W:
        pad_param = ((0,0),(padd_1,padd_2),(0,0))
    else:
        pad_param = ((padd_1,padd_2),(0,0),(0,0))

    image = np.pad(image, pad_param, 'constant', constant_values=pad_value)
    return image



def box_in_box(small_box, big_box):
    y1,y2,x1,x2 = small_box
    y1_b, _, x1_b, _ = big_box
    y1,y2,x1,x2 = y1 - y1_b ,y2 - y1_b, x1 - x1_b ,x2 - x1_b
    return (y1,y2,x1,x2 )



def shuffle_image(image, N):
    height, width = image.shape[:2]
    
    block_height = height // N
    block_width = width // N
    blocks = []
    
    for i in range(N):
        for j in range(N):
            block = image[i*block_height:(i+1)*block_height, j*block_width:(j+1)*block_width]
            blocks.append(block)
    
    np.random.shuffle(blocks)
    shuffled_image = np.zeros((height, width, 3), dtype=np.uint8)

    for i in range(N):
        for j in range(N):
            shuffled_image[i*block_height:(i+1)*block_height, j*block_width:(j+1)*block_width] = blocks[i*N+j]
    return shuffled_image


def get_mosaic_mask(image, fg_mask, N=16, ratio = 0.5):
    ids = [i for i in range(N * N)]
    masked_number = int(N * N * ratio)
    masked_id = np.random.choice(ids, masked_number, replace=False)
    

    
    height, width = image.shape[:2]
    mask = np.ones((height, width))
    
    block_height = height // N
    block_width = width // N
    
    b_id = 0
    for i in range(N):
        for j in range(N):
            if b_id in masked_id:
                mask[i*block_height:(i+1)*block_height, j*block_width:(j+1)*block_width] = mask[i*block_height:(i+1)*block_height, j*block_width:(j+1)*block_width] * 0
            b_id += 1
    mask = mask * fg_mask
    mask3 = np.stack([mask,mask,mask],-1).copy().astype(np.uint8)
    noise = q_x(image)
    noise_mask = image * mask3 + noise * (1-mask3)
    return noise_mask

def extract_canney_noise(image, mask, dilate=True):
    h,w = image.shape[0],image.shape[1]
    mask = cv2.resize(mask.astype(np.uint8),(w,h)) > 0.5
    kernel = np.ones((8, 8), dtype=np.uint8)
    mask =  cv2.erode(mask.astype(np.uint8), kernel, 10)

    canny = cv2.Canny(image, 50,100) * mask
    kernel = np.ones((8, 8), dtype=np.uint8)
    mask = (cv2.dilate(canny, kernel, 5) > 128).astype(np.uint8)
    mask = np.stack([mask,mask,mask],-1)

    pure_noise = q_x(image, t=1) * 0 + 255
    canny_noise = mask * image + (1-mask) * pure_noise
    return canny_noise


def get_random_structure(size):
    choice = np.random.randint(1, 5)

    if choice == 1:
        return cv2.getStructuringElement(cv2.MORPH_RECT, (size, size))
    elif choice == 2:
        return cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (size, size))
    elif choice == 3:
        return cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (size, size//2))
    elif choice == 4:
        return cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (size//2, size))

def random_dilate(seg, min=3, max=10):
    size = np.random.randint(min, max)
    kernel = get_random_structure(size)
    seg = cv2.dilate(seg,kernel,iterations = 1)
    return seg

def random_erode(seg, min=3, max=10):
    size = np.random.randint(min, max)
    kernel = get_random_structure(size)
    seg = cv2.erode(seg,kernel,iterations = 1)
    return seg

def compute_iou(seg, gt):
    intersection = seg*gt
    union = seg+gt
    return (np.count_nonzero(intersection) + 1e-6) / (np.count_nonzero(union) + 1e-6)


def select_max_region(mask):
    nums, labels, stats, centroids = cv2.connectedComponentsWithStats(mask, connectivity=8)
    background = 0
    for row in range(stats.shape[0]):
        if stats[row, :][0] == 0 and stats[row, :][1] == 0:
            background = row
    stats_no_bg = np.delete(stats, background, axis=0)
    max_idx = stats_no_bg[:, 4].argmax()
    max_region = np.where(labels==max_idx+1, 1, 0)

    return max_region.astype(np.uint8)



def perturb_mask(gt, min_iou = 0.3,  max_iou = 0.99):
    iou_target = np.random.uniform(min_iou, max_iou)
    h, w = gt.shape
    gt = gt.astype(np.uint8)
    seg = gt.copy()
    
    # Rare case
    if h <= 2 or w <= 2:
        print('GT too small, returning original')
        return seg

    # Do a bunch of random operations
    for _ in range(250):
        for _ in range(4):
            lx, ly = np.random.randint(w), np.random.randint(h)
            lw, lh = np.random.randint(lx+1,w+1), np.random.randint(ly+1,h+1)

            # Randomly set one pixel to 1/0. With the following dilate/erode, we can create holes/external regions
            if np.random.rand() < 0.1:
                cx = int((lx + lw) / 2)
                cy = int((ly + lh) / 2)
                seg[cy, cx] = np.random.randint(2) * 255

            # Dilate/erode
            if np.random.rand() < 0.5:
                seg[ly:lh, lx:lw] = random_dilate(seg[ly:lh, lx:lw])
            else:
                seg[ly:lh, lx:lw] = random_erode(seg[ly:lh, lx:lw])
            
            seg = np.logical_or(seg, gt).astype(np.uint8)
            #seg = select_max_region(seg) 

        if compute_iou(seg, gt) < iou_target:
            break
    seg = select_max_region(seg.astype(np.uint8)) 
    return seg.astype(np.uint8)


def q_x(x_0,t=65):
    '''Adding noise for and given image.'''
    x_0 = torch.from_numpy(x_0).float() / 127.5 - 1
    num_steps = 100
    
    betas = torch.linspace(-6,6,num_steps)
    betas = torch.sigmoid(betas)*(0.5e-2 - 1e-5)+1e-5

    alphas = 1-betas
    alphas_prod = torch.cumprod(alphas,0)
    
    alphas_prod_p = torch.cat([torch.tensor([1]).float(),alphas_prod[:-1]],0)
    alphas_bar_sqrt = torch.sqrt(alphas_prod)
    one_minus_alphas_bar_log = torch.log(1 - alphas_prod)
    one_minus_alphas_bar_sqrt = torch.sqrt(1 - alphas_prod)
    
    noise = torch.randn_like(x_0)
    alphas_t = alphas_bar_sqrt[t]
    alphas_1_m_t = one_minus_alphas_bar_sqrt[t]
    return (alphas_t * x_0 + alphas_1_m_t * noise).numpy()  * 127.5 + 127.5 


def extract_target_boundary(img, target_mask):
    Ksize = 3
    sobelx = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=Ksize)
    sobely = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=Ksize)

    # sobel-x
    sobel_X = cv2.convertScaleAbs(sobelx)
    # sobel-y
    sobel_Y = cv2.convertScaleAbs(sobely)
    # sobel-xy
    scharr = cv2.addWeighted(sobel_X, 0.5, sobel_Y, 0.5, 0)
    scharr = np.max(scharr,-1).astype(np.float32)/255
    scharr = scharr *  target_mask.astype(np.float32)
    return scharr


def init_model_ddim():
    cv2.setNumThreads(0)
    cv2.ocl.setUseOpenCL(False)

    disable_verbosity()

    model = create_model(config_path).to(device)
    model.load_state_dict(load_state_dict(anydoor_config.model_ckpt, location=device))
    ddim_sampler = DDIMSampler(model)

    if anydoor_config.use_interactive_seg == True:
        from iseg.coarse_mask_refine_util import BaselineModel
        model_path = 'modules/try_on/anydoor/iseg/coarse_mask_refine.pth'
        iseg_model = BaselineModel().eval()
        weights = torch.load(model_path , map_location=device)['state_dict']
        iseg_model.load_state_dict(weights, strict= True)

    return model, ddim_sampler

model, ddim_sampler = init_model_ddim()
automasker = AutoMasker(
    densepose_ckpt=config.service.catvton.model_path.densepose,
    schp_ckpt=config.service.catvton.model_path.schp,
    device='cuda', 
)
mask_processor = VaeImageProcessor(vae_scale_factor=8, do_normalize=False, do_binarize=True, do_convert_grayscale=True)

if torch.cuda.is_available():
    torch.cuda.empty_cache()
    torch.cuda.ipc_collect()

def crop_back( pred, tar_image,  extra_sizes, tar_box_yyxx_crop):
    H1, W1, H2, W2 = extra_sizes
    y1,y2,x1,x2 = tar_box_yyxx_crop    
    pred = cv2.resize(pred, (W2, H2))
    m = 3 # maigin_pixel

    if W1 == H1:
        tar_image[y1+m :y2-m, x1+m:x2-m, :] =  pred[m:-m, m:-m]
        return tar_image

    if W1 < W2:
        pad1 = int((W2 - W1) / 2)
        pad2 = W2 - W1 - pad1
        pred = pred[:,pad1: -pad2, :]
    else:
        pad1 = int((H2 - H1) / 2)
        pad2 = H2 - H1 - pad1
        pred = pred[pad1: -pad2, :, :]
    tar_image[y1+m :y2-m, x1+m:x2-m, :] =  pred[m:-m, m:-m]
    return tar_image


def process_pairs(ref_image, ref_mask, tar_image, tar_mask, max_ratio = 0.8, enable_shape_control = False):
    # ========= Reference ===========
    # ref expand 
    ref_box_yyxx = get_bbox_from_mask(ref_mask)

    # ref filter mask 
    ref_mask_3 = np.stack([ref_mask,ref_mask,ref_mask],-1)
    masked_ref_image = ref_image * ref_mask_3 + np.ones_like(ref_image) * 255 * (1-ref_mask_3)

    y1,y2,x1,x2 = ref_box_yyxx
    masked_ref_image = masked_ref_image[y1:y2,x1:x2,:]
    ref_mask = ref_mask[y1:y2,x1:x2]

    ratio = np.random.randint(11, 15) / 10 #11,13
    masked_ref_image, ref_mask = expand_image_mask(masked_ref_image, ref_mask, ratio=ratio)
    ref_mask_3 = np.stack([ref_mask,ref_mask,ref_mask],-1)

    # to square and resize
    masked_ref_image = pad_to_square(masked_ref_image, pad_value = 255, random = False)
    masked_ref_image = cv2.resize(masked_ref_image.astype(np.uint8), (224,224) ).astype(np.uint8)

    ref_mask_3 = pad_to_square(ref_mask_3 * 255, pad_value = 0, random = False)
    ref_mask_3 = cv2.resize(ref_mask_3.astype(np.uint8), (224,224) ).astype(np.uint8)
    ref_mask = ref_mask_3[:,:,0]

    # collage aug 
    masked_ref_image_compose, ref_mask_compose =  masked_ref_image, ref_mask
    ref_mask_3 = np.stack([ref_mask_compose,ref_mask_compose,ref_mask_compose],-1)
    ref_image_collage = sobel(masked_ref_image_compose, ref_mask_compose/255)

    # ========= Target ===========
    tar_box_yyxx = get_bbox_from_mask(tar_mask)
    tar_box_yyxx = expand_bbox(tar_mask, tar_box_yyxx, ratio=[1.1,1.2]) #1.1  1.3
    tar_box_yyxx_full = tar_box_yyxx
    
    # crop
    tar_box_yyxx_crop =  expand_bbox(tar_image, tar_box_yyxx, ratio=[1.3, 3.0])   
    tar_box_yyxx_crop = box2squre(tar_image, tar_box_yyxx_crop) # crop box
    y1,y2,x1,x2 = tar_box_yyxx_crop

    cropped_target_image = tar_image[y1:y2,x1:x2,:]
    cropped_tar_mask = tar_mask[y1:y2,x1:x2]

    tar_box_yyxx = box_in_box(tar_box_yyxx, tar_box_yyxx_crop)
    y1,y2,x1,x2 = tar_box_yyxx

    # collage
    ref_image_collage = cv2.resize(ref_image_collage.astype(np.uint8), (x2-x1, y2-y1))
    ref_mask_compose = cv2.resize(ref_mask_compose.astype(np.uint8), (x2-x1, y2-y1))
    ref_mask_compose = (ref_mask_compose > 128).astype(np.uint8)

    collage = cropped_target_image.copy() 
    collage[y1:y2,x1:x2,:] = ref_image_collage

    collage_mask = cropped_target_image.copy() * 0.0
    collage_mask[y1:y2,x1:x2,:] = 1.0
    if enable_shape_control:
        collage_mask = np.stack([cropped_tar_mask,cropped_tar_mask,cropped_tar_mask],-1)

    # the size before pad
    H1, W1 = collage.shape[0], collage.shape[1]

    cropped_target_image = pad_to_square(cropped_target_image, pad_value = 0, random = False).astype(np.uint8)
    collage = pad_to_square(collage, pad_value = 0, random = False).astype(np.uint8)
    collage_mask = pad_to_square(collage_mask, pad_value = 2, random = False).astype(np.uint8)

    # the size after pad
    H2, W2 = collage.shape[0], collage.shape[1]

    cropped_target_image = cv2.resize(cropped_target_image.astype(np.uint8), (512,512)).astype(np.float32)
    collage = cv2.resize(collage.astype(np.uint8), (512,512)).astype(np.float32)
    collage_mask  = cv2.resize(collage_mask.astype(np.uint8), (512,512),  interpolation = cv2.INTER_NEAREST).astype(np.float32)
    collage_mask[collage_mask == 2] = -1

    masked_ref_image = masked_ref_image  / 255 
    cropped_target_image = cropped_target_image / 127.5 - 1.0
    collage = collage / 127.5 - 1.0 
    collage = np.concatenate([collage, collage_mask[:,:,:1]  ] , -1)
    
    item = dict(ref=masked_ref_image.copy(), jpg=cropped_target_image.copy(), hint=collage.copy(), 
                extra_sizes=np.array([H1, W1, H2, W2]), 
                tar_box_yyxx_crop=np.array( tar_box_yyxx_crop ),
                tar_box_yyxx=np.array(tar_box_yyxx_full),
                 ) 
    return item


def mask_image(image, mask):
    blanc = np.ones_like(image) * 255
    mask = np.stack([mask,mask,mask],-1) / 255
    masked_image = mask * ( 0.5 * blanc + 0.5 * image) + (1-mask) * image
    return masked_image.astype(np.uint8)


def array_base64_encode(arr):
    bio = BytesIO()
    np.savetxt(bio, arr)
    return base64.b64encode(bio.getvalue())


def array_base64_decode(b64):
    b64_decode = base64.b64decode(b64)
    return np.loadtxt(BytesIO(b64_decode))


def inference_single_image(ref_image, 
                           ref_mask, 
                           tar_image, 
                           tar_mask, 
                           strength, 
                           ddim_steps, 
                           scale, 
                           seed,
                           enable_shape_control,
                           ):
    raw_background = tar_image.copy()
    item = process_pairs(ref_image, ref_mask, tar_image, tar_mask, enable_shape_control = enable_shape_control)

    ref = item['ref']
    hint = item['hint']
    num_samples = 1

    control = torch.from_numpy(hint.copy()).float().to(device)
    control = torch.stack([control for _ in range(num_samples)], dim=0)
    control = einops.rearrange(control, 'b h w c -> b c h w').clone()


    clip_input = torch.from_numpy(ref.copy()).float().to(device)
    clip_input = torch.stack([clip_input for _ in range(num_samples)], dim=0)
    clip_input = einops.rearrange(clip_input, 'b h w c -> b c h w').clone()

    H,W = 512,512

    cond = {"c_concat": [control], "c_crossattn": [model.get_learned_conditioning( clip_input )]}
    un_cond = {"c_concat": [control], 
               "c_crossattn": [model.get_learned_conditioning([torch.zeros((1,3,224,224))] * num_samples)]}
    shape = (4, H // 8, W // 8)

    # if save_memory:
    #     model.low_vram_shift(is_diffusing=True)

    model.control_scales = ([strength] * 13)
    samples, _ = ddim_sampler.sample(ddim_steps, num_samples,
                                     shape, cond, verbose=False, eta=0,
                                     unconditional_guidance_scale=scale,
                                     unconditional_conditioning=un_cond)

    # if save_memory:
    #     model.low_vram_shift(is_diffusing=False)

    x_samples = model.decode_first_stage(samples)
    x_samples = (einops.rearrange(x_samples, 'b c h w -> b h w c') * 127.5 + 127.5).cpu().numpy()

    result = x_samples[0][:,:,::-1]
    result = np.clip(result,0,255)

    pred = x_samples[0]
    pred = np.clip(pred,0,255)[1:,:,:]
    sizes = item['extra_sizes']
    tar_box_yyxx_crop = item['tar_box_yyxx_crop'] 
    tar_image = crop_back(pred, tar_image, sizes, tar_box_yyxx_crop) 

    # keep background unchanged
    y1,y2,x1,x2 = item['tar_box_yyxx']
    raw_background[y1:y2, x1:x2, :] = tar_image[y1:y2, x1:x2, :]
    raw_background = torch.from_numpy(raw_background).permute(2, 0, 1)
    raw_background = raw_background.permute(1, 2, 0).numpy()

    localdir = f"statics/image/{datetime.now().strftime('%Y-%m-%d')}"
    os.makedirs(localdir, exist_ok=True)
    localfile = f"{localdir}/{uuid.uuid4()}.png"

    Image.fromarray(raw_background).save(localfile)

    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()

    return localfile


@router.post("/tryon_url")
def tryon_url(cloth_image: UploadFile, cloth_mask: UploadFile, person_image: UploadFile, person_mask: UploadFile,
              strength=Form(None), num_inference_steps=Form(None), guidance_scale=Form(None), seed=Form(None), enable_shape_control=Form(None)):
    localdir = f"statics/upload/{datetime.now().strftime('%Y-%m-%d')}/{uuid.uuid4()}"
    os.makedirs(localdir, exist_ok=True)

    with open(f"{localdir}/{cloth_image.filename}", "wb") as f:
        f.write(cloth_image.file.read())
    with open(f"{localdir}/{cloth_mask.filename}", "wb") as f:
        f.write(cloth_mask.file.read())
    with open(f"{localdir}/{person_image.filename}", "wb") as f:
        f.write(person_image.file.read())
    with open(f"{localdir}/{person_mask.filename}", "wb") as f:
        f.write(person_mask.file.read())

    refs_image = np.asarray(Image.open(f"{localdir}/{cloth_image.filename}"))
    refs_mask = np.asarray(Image.open(f"{localdir}/{cloth_mask.filename}"))
    refs_mask = np.where(refs_mask > 128, 1, 0).astype(np.uint8)
    if refs_mask.sum() == 0:
        refs_mask = automasker(Image.open(f"{localdir}/{cloth_image.filename}"), "upper")['mask']
        # refs_mask = mask_processor.blur(refs_mask, blur_factor=9)
        refs_mask = np.asarray(refs_mask)
        refs_mask = np.where(refs_mask > 128, 1, 0).astype(np.uint8)

    base_image = np.asarray(Image.open(f"{localdir}/{person_image.filename}"))
    base_mask = np.asarray(Image.open(f"{localdir}/{person_mask.filename}"))
    base_mask = np.where(base_mask > 128, 1, 0).astype(np.uint8)
    if base_mask.sum() == 0:
        base_mask = automasker(Image.open(f"{localdir}/{person_image.filename}"), "upper")['mask']
        # base_mask = mask_processor.blur(base_mask, blur_factor=9)
        base_mask = np.asarray(base_mask)
        base_mask = np.where(base_mask > 128, 1, 0).astype(np.uint8)

    localfile = inference_single_image(ref_image=refs_image.copy(), 
                                       ref_mask=refs_mask.copy(), 
                                       tar_image=base_image.copy(),
                                      tar_mask=base_mask.copy(),
                                      strength=float(strength) if strength else 1.0, 
                                      ddim_steps=int(num_inference_steps) if num_inference_steps else 30,
                                      scale=float(guidance_scale) if guidance_scale else 2.5,
                                      seed=int(seed) if seed else 42,
                                      enable_shape_control=bool(enable_shape_control) if enable_shape_control else False
                                      )
    return localfile


@router.post("/tryon_file")
def tryon_file(cloth_image: UploadFile, cloth_mask: UploadFile, person_image: UploadFile, person_mask: UploadFile,
              strength=Form(None), num_inference_steps=Form(None), guidance_scale=Form(None), seed=Form(None), enable_shape_control=Form(None)):
    return FileResponse(tryon_url(cloth_image, cloth_mask, person_image, person_mask, strength, num_inference_steps, guidance_scale, seed, enable_shape_control), media_type="image/png")
