from fastapi import APIRouter, Request, HTTPException, Response, File, UploadFile, Form
from fastapi.responses import FileResponse

from datetime import datetime

import os
import uuid
import argparse
import numpy as np
import torch
from diffusers.image_processor import VaeImageProcessor
from huggingface_hub import snapshot_download
from PIL import Image

from model.cloth_masker import AutoMasker, vis_mask
from model.pipeline import CatVTONPipeline
from utils import init_weight_dtype, resize_and_crop, resize_and_padding

from configs import config


router = APIRouter(
    prefix='/try_on/catvton',
    tags = ['虚拟试穿']
)

def parse_args():
    parser = argparse.ArgumentParser(description="Simple example of a training script.")
    parser.add_argument(
        "--width",
        type=int,
        default=768,
        help=(
            "The resolution for input images, all the images in the train/validation dataset will be resized to this"
            " resolution"
        ),
    )
    parser.add_argument(
        "--height",
        type=int,
        default=1024,
        help=(
            "The resolution for input images, all the images in the train/validation dataset will be resized to this"
            " resolution"
        ),
    )
    parser.add_argument(
        "--repaint", 
        action="store_true", 
        help="Whether to repaint the result image with the original background."
    )
    parser.add_argument(
        "--allow_tf32",
        action="store_true",
        default=True,
        help=(
            "Whether or not to allow TF32 on Ampere GPUs. Can be used to speed up training. For more information, see"
            " https://pytorch.org/docs/stable/notes/cuda.html#tensorfloat-32-tf32-on-ampere-devices"
        ),
    )
    parser.add_argument(
        "--mixed_precision",
        type=str,
        default="bf16",
        choices=["no", "fp16", "bf16"],
        help=(
            "Whether to use mixed precision. Choose between fp16 and bf16 (bfloat16). Bf16 requires PyTorch >="
            " 1.10.and an Nvidia Ampere GPU.  Default to the value of accelerate config of the current system or the"
            " flag passed with the `accelerate.launch` command. Use this argument to override the accelerate config."
        ),
    )
    
    args = parser.parse_args()
    env_local_rank = int(os.environ.get("LOCAL_RANK", -1))
    if env_local_rank != -1 and env_local_rank != args.local_rank:
        args.local_rank = env_local_rank

    return args

def image_grid(imgs, rows, cols):
    assert len(imgs) == rows * cols

    w, h = imgs[0].size
    grid = Image.new("RGB", size=(cols * w, rows * h))

    for i, img in enumerate(imgs):
        grid.paste(img, box=(i % cols * w, i // cols * h))
    return grid


args = parse_args()

# Pipeline
pipeline = CatVTONPipeline(
    base_ckpt=config.service.catvton.model_path.inpainting,
    attn_ckpt=config.service.catvton.model_path.catvton,
    attn_ckpt_version="mix",
    weight_dtype=init_weight_dtype(args.mixed_precision),
    use_tf32=args.allow_tf32,
    device='cuda'
)
# AutoMasker
mask_processor = VaeImageProcessor(vae_scale_factor=8, do_normalize=False, do_binarize=True, do_convert_grayscale=True)
automasker = AutoMasker(
    densepose_ckpt=config.service.catvton.model_path.densepose,
    schp_ckpt=config.service.catvton.model_path.schp,
    device='cuda', 
)


def generate_image(
    person_image,
    cloth_image,
    num_inference_steps,
    guidance_scale,
    seed
):
    localdir = f"statics/image/{datetime.now().strftime('%Y-%m-%d')}"
    os.makedirs(localdir, exist_ok=True)
    localfile = f"{localdir}/{uuid.uuid4()}.png"

    mask = None
    generator = None

    if seed != -1:
        generator = torch.Generator(device='cuda').manual_seed(seed)

    person_image = Image.open(person_image).convert("RGB")
    cloth_image = Image.open(cloth_image).convert("RGB")
    person_image = resize_and_crop(person_image, (args.width, args.height))
    cloth_image = resize_and_padding(cloth_image, (args.width, args.height))
    
    if mask is not None:
        mask = resize_and_crop(mask, (args.width, args.height))
    else:
        mask = automasker(
            person_image,
            "upper"
        )['mask']
    mask = mask_processor.blur(mask, blur_factor=9)

    result_image = pipeline(
        image=person_image,
        condition_image=cloth_image,
        mask=mask,
        num_inference_steps=num_inference_steps,
        guidance_scale=guidance_scale,
        generator=generator
    )[0]
    result_image.save(localfile)

    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()

    return localfile


@router.post("/tryon_url")
def tryon_url(cloth_image: UploadFile, cloth_mask: UploadFile, person_image: UploadFile, person_mask: UploadFile,
              strength=Form(None), num_inference_steps=Form(None), guidance_scale=Form(None), seed=Form(None), enable_shape_control=Form(None)):
    localdir = f"statics/upload/{datetime.now().strftime('%Y-%m-%d')}/{uuid.uuid4()}"
    os.makedirs(localdir, exist_ok=True)

    with open(f"{localdir}/{cloth_image.filename}", "wb") as f:
        f.write(cloth_image.file.read())
    with open(f"{localdir}/{cloth_mask.filename}", "wb") as f:
        f.write(cloth_mask.file.read())
    with open(f"{localdir}/{person_image.filename}", "wb") as f:
        f.write(person_image.file.read())
    with open(f"{localdir}/{person_mask.filename}", "wb") as f:
        f.write(person_mask.file.read())

    localfile = generate_image(person_image=f"{localdir}/{person_image.filename}",
                                cloth_image=f"{localdir}/{cloth_image.filename}", 
                                num_inference_steps=int(num_inference_steps) if num_inference_steps else 30,
                                guidance_scale=float(guidance_scale) if guidance_scale else 2.5,
                                seed=int(seed) if seed else 42
                                )
    return localfile


@router.post("/tryon_file")
def tryon_file(cloth_image: UploadFile, cloth_mask: UploadFile, person_image: UploadFile, person_mask: UploadFile,
              strength=Form(None), num_inference_steps=Form(None), guidance_scale=Form(None), seed=Form(None), enable_shape_control=Form(None)):
    return FileResponse(tryon_url(cloth_image, cloth_mask, person_image, person_mask, strength, num_inference_steps, guidance_scale, seed, enable_shape_control), media_type="image/png")
