from fastapi import APIRouter, Request, HTTPException, Response, UploadFile, Form

import os
import uuid
from datetime import datetime
import re
import torch
import torchvision.transforms as T
from openai import OpenAI

from PIL import Image
from transformers import AutoModel, AutoTokenizer, LogitsProcessor
from torchvision.transforms.functional import InterpolationMode

from configs import config

router = APIRouter(
    prefix='/vision_language/internvl',
    tags = ['图片识别']
)

# GiteeAI 平台部署加速
# model_path = 'hf-models/Mini-InternVL-Chat-2B-V1-5'

# 下载到本地
# model_path = 'models/OpenGVLab/Mini-InternVL-Chat-4B-V1-5'

# 使用配置文件
model_path = config.service.internvl.model_path

model = AutoModel.from_pretrained(model_path, trust_remote_code=True).to(torch.float16)
tokenizer = AutoTokenizer.from_pretrained(model_path, trust_remote_code=True)
tokenizer.pad_token = "[PAD]"
tokenizer.padding_side = "left"
model = model.to(device="cuda", dtype=torch.float16)
model.eval()


def build_transform(input_size):
    IMAGENET_MEAN = (0.485, 0.456, 0.406)
    IMAGENET_STD = (0.229, 0.224, 0.225)
    MEAN, STD = IMAGENET_MEAN, IMAGENET_STD
    transform = T.Compose([
        T.Lambda(lambda img: img.convert('RGB') if img.mode != 'RGB' else img),
        T.Resize((input_size, input_size), interpolation=InterpolationMode.BICUBIC),
        T.ToTensor(),
        T.Normalize(mean=MEAN, std=STD)
    ])
    return transform


def find_closest_aspect_ratio(aspect_ratio, target_ratios, width, height, image_size):
    best_ratio_diff = float('inf')
    best_ratio = (1, 1)
    area = width * height
    for ratio in target_ratios:
        target_aspect_ratio = ratio[0] / ratio[1]
        ratio_diff = abs(aspect_ratio - target_aspect_ratio)
        if ratio_diff < best_ratio_diff:
            best_ratio_diff = ratio_diff
            best_ratio = ratio
        elif ratio_diff == best_ratio_diff:
            if area > 0.5 * image_size * image_size * ratio[0] * ratio[1]:
                best_ratio = ratio
    return best_ratio


def dynamic_preprocess(image, min_num=1, max_num=6, image_size=448, use_thumbnail=False):
    orig_width, orig_height = image.size
    aspect_ratio = orig_width / orig_height

    # calculate the existing image aspect ratio
    target_ratios = set(
        (i, j) for n in range(min_num, max_num + 1) for i in range(1, n + 1) for j in range(1, n + 1) if
        i * j <= max_num and i * j >= min_num)
    target_ratios = sorted(target_ratios, key=lambda x: x[0] * x[1])

    # find the closest aspect ratio to the target
    target_aspect_ratio = find_closest_aspect_ratio(
        aspect_ratio, target_ratios, orig_width, orig_height, image_size)

    # calculate the target width and height
    target_width = image_size * target_aspect_ratio[0]
    target_height = image_size * target_aspect_ratio[1]
    blocks = target_aspect_ratio[0] * target_aspect_ratio[1]

    # resize the image
    resized_img = image.resize((target_width, target_height))
    processed_images = []
    for i in range(blocks):
        box = (
            (i % (target_width // image_size)) * image_size,
            (i // (target_width // image_size)) * image_size,
            ((i % (target_width // image_size)) + 1) * image_size,
            ((i // (target_width // image_size)) + 1) * image_size
        )
        # split the image
        split_img = resized_img.crop(box)
        processed_images.append(split_img)
    assert len(processed_images) == blocks
    if use_thumbnail and len(processed_images) != 1:
        thumbnail_img = image.resize((image_size, image_size))
        processed_images.append(thumbnail_img)
    return processed_images


def dynamic_preprocess(image, min_num=1, max_num=6, image_size=448, use_thumbnail=False):
    orig_width, orig_height = image.size
    aspect_ratio = orig_width / orig_height

    # calculate the existing image aspect ratio
    target_ratios = set(
        (i, j) for n in range(min_num, max_num + 1) for i in range(1, n + 1) for j in range(1, n + 1) if
        i * j <= max_num and i * j >= min_num)
    target_ratios = sorted(target_ratios, key=lambda x: x[0] * x[1])

    # find the closest aspect ratio to the target
    target_aspect_ratio = find_closest_aspect_ratio(
        aspect_ratio, target_ratios, orig_width, orig_height, image_size)

    # calculate the target width and height
    target_width = image_size * target_aspect_ratio[0]
    target_height = image_size * target_aspect_ratio[1]
    blocks = target_aspect_ratio[0] * target_aspect_ratio[1]

    # resize the image
    resized_img = image.resize((target_width, target_height))
    processed_images = []
    for i in range(blocks):
        box = (
            (i % (target_width // image_size)) * image_size,
            (i // (target_width // image_size)) * image_size,
            ((i % (target_width // image_size)) + 1) * image_size,
            ((i // (target_width // image_size)) + 1) * image_size
        )
        # split the image
        split_img = resized_img.crop(box)
        processed_images.append(split_img)
    assert len(processed_images) == blocks
    if use_thumbnail and len(processed_images) != 1:
        thumbnail_img = image.resize((image_size, image_size))
        processed_images.append(thumbnail_img)
    return processed_images


def load_image(image_file, input_size=448, max_num=6):
    image = image_file.convert('RGB')
    transform = build_transform(input_size=input_size)
    images = dynamic_preprocess(image, image_size=input_size, use_thumbnail=True, max_num=max_num)
    pixel_values = [transform(image) for image in images]
    pixel_values = torch.stack(pixel_values)
    return pixel_values



@router.post("/chat_with_image")
def chat_with_image(image: UploadFile, user_input=Form(None)):
    params = {
        'top_p': 0.8,
        'top_k': 100,
        'temperature': 0.7,
        'repetition_penalty': 1.05,
        "max_new_tokens": 1024
    }

    localdir = f"statics/upload/{datetime.now().strftime('%Y-%m-%d')}/{uuid.uuid4()}"
    os.makedirs(localdir, exist_ok=True)

    with open(f"{localdir}/{image.filename}", "wb") as f:
        f.write(image.file.read())

    answer, context= model.chat(
        tokenizer,
        load_image(Image.open(f"{localdir}/{image.filename}")).to(torch.float16).cuda(),
        user_input,
        params,
        return_history=True
    )
    answer = re.sub(r'(<box>.*</box>)', '', answer).replace('<ref>', '').replace('</ref>', '').replace('<box>', '').replace('</box>', '')

    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()

    return answer


@router.post("/marketing_documents")
def marketing_documents(image: UploadFile,
                        product_name=Form(None),
                        product_tags=Form(None),
                        product_gender=Form(None),
                        product_season=Form(None),
                        product_price=Form(None),
                        product_style=Form(None),
                        product_material=Form(None),
                        product_advantage=Form(None),
                        product_description=Form(None)
                        ):
    image_description = chat_with_image(image, "请详细描述这幅图片，精准捕获图片中服装的每一个细节")

    messages = [{
                "role": "system",
                "content": "你是一位优秀的服装商品智能销售专家，你需要推销一件服装，你需要放大商品的优点，激发用户的购买欲望！"
            }, {
                "role": "system",
                "content": image_description
            }, {
                "role": "user",
                "content": """这件服装商品的详细信息如下：
商品名称：{product_name}
商品标签：{product_tags}
商品类型：{product_gender}
适合季节：{product_season}
商品价格：{product_price}
设计风格：{product_style}
服装材质：{product_material}
商品描述：{product_description}。

请写一段电商平台的种草文案。""".format(
            product_name = product_name,
            product_tags = product_tags,
            product_gender = product_gender,
            product_season = product_season,
            product_price = product_price,
            product_style = product_style,
            product_material = product_material,
            product_advantage = product_advantage,
            product_description = product_description
        )
            }]

    client = OpenAI(api_key=os.getenv(config.service.chat.api_key_env, default=config.service.chat.api_key_env), base_url=config.service.chat.base_url)

    answer = client.chat.completions.create(
        model="glm-4",
        messages=messages,
        stream=False,
        max_tokens=4096,
        temperature=0.7,
        presence_penalty=1.2,
        top_p=0.8,
    ).choices[0].message.content

    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()

    return image_description + "\n\n" + answer
