from fastapi import APIRouter, Request, HTTPException, Response
from fastapi.responses import FileResponse

import os
import re
import json
import torch
import numpy as np
import uuid
import soundfile

from datetime import datetime

from configs import config

import ChatTTS
chat = ChatTTS.Chat()

# GiteeAI 平台部署加速
# chat.load_models(source='huggingface', force_redownload=False)

# 下载到本地
# chat.load_models(source='local', force_redownload=False, model_path='models/2Noise/ChatTTS')

# 使用配置文件
chat.load_models(source=config.service.chattts.source, force_redownload=config.service.chattts.force_redownload, model_path=config.service.chattts.model_path)

router = APIRouter(
    prefix='/tts/chattts',
    tags = ['语音生成']
)

def generate_audio(text, temperature=0.3, top_P=0.7, top_K=20, audio_seed_input=42, text_seed_input=42, refine_text_flag=True):
    torch.manual_seed(audio_seed_input)
    rand_spk = torch.randn(768)
    params_infer_code = {
        'spk_emb': rand_spk, 
        'temperature': temperature,
        'top_P': top_P,
        'top_K': top_K,
        }
    params_refine_text = {'prompt': '[oral_2][laugh_0][break_6]'}
    
    torch.manual_seed(text_seed_input)

    if refine_text_flag:
        text = chat.infer(text, 
                          skip_refine_text=False,
                          refine_text_only=True,
                          params_refine_text=params_refine_text,
                          params_infer_code=params_infer_code
                          )
    
    wav = chat.infer(text, 
                     skip_refine_text=True, 
                     params_refine_text=params_refine_text, 
                     params_infer_code=params_infer_code
                     )
    
    audio_data = np.array(wav[0]).flatten()

    localdir = f"statics/audio/{datetime.now().strftime('%Y-%m-%d')}"
    os.makedirs(localdir, exist_ok=True)
    localfile = f"{localdir}/{uuid.uuid4()}.wav"
    
    soundfile.write(localfile, audio_data, 24000)

    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()
        
    return localfile


@router.post("/generate_audio_url")
def generate_audio_url(request: dict):
    try:
        localfile = generate_audio(text=request["text"],
                            temperature=float(request.get("temperature")) if request.get("temperature") else 0.3,
                            top_P=float(request.get("top_P")) if request.get("top_P") else 0.7,
                            top_K=float(request.get("top_K")) if request.get("top_K") else 20,
                            audio_seed_input=float(request.get("audio_seed_input")) if request.get("audio_seed_input") else 42,
                            text_seed_input=float(request.get("text_seed_input")) if request.get("text_seed_input") else 42
                            )
    except:
        localfile = generate_audio(text=request["text"])

    return localfile


@router.post("/generate_audio_file")
def generate_audio_file(request: dict):
    return FileResponse(generate_audio_url(request), media_type="audio/wav")
