from fastapi import APIRouter, Request, HTTPException, Response, UploadFile

import torch

import base64
import os, sys
import uuid
from datetime import datetime

from funasr import AutoModel
from funasr.utils.postprocess_utils import rich_transcription_postprocess

from modules.utils.FileUtil import fetch_file
from configs import config


router = APIRouter(
    prefix='/asr/funasr',
    tags = ['语音识别']
)

model = AutoModel(
    model="iic/SenseVoiceSmall",
    vad_model="fsmn-vad",
    vad_kwargs={"max_single_segment_time": 30000},
    device=config.service.funasr.device,
)


def audio_asr(source):
    localdir = f"upload/{datetime.now().strftime('%Y-%m-%d')}"
    os.makedirs(f"{config.setting.statics.path}/{localdir}", exist_ok=True)
    localfile = f"{localdir}/{uuid.uuid4()}"

    fetch_file(source, f"{config.setting.statics.path}/{localfile}")

    res = model.generate(
        input=f"{config.setting.statics.path}/{localfile}",
        cache={},
        language="auto",  # "zn", "en", "yue", "ja", "ko", "nospeech"
        use_itn=True,
        batch_size_s=60,
        merge_vad=True,  #
        merge_length_s=15,
    )

    result = rich_transcription_postprocess(res[0]["text"])

    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()

    return result


@router.post("/audio/translations")
def audio_translations(request: dict):
    return {"text": audio_asr(request["file"])}


"""
自动语音识别，将语音信息转换成文本数据
"""
@router.post("/v1")
def audio_to_text(request: dict):
    # 先将语音数据保存成本地音频文件
    localdir = f"upload/{datetime.now().strftime('%Y-%m-%d')}"
    os.makedirs(f"{config.setting.statics.path}/{localdir}", exist_ok=True)
    localfile = f"{config.setting.statics.path}/{localdir}/{uuid.uuid4()}"
    with open(localfile, "wb") as f:
        f.write(base64.b64decode(request["audio"]))

    # 模型对本地音频文件进行识别，生成文本内容
    res = model.generate(
        input=localfile,
        cache={},
        language="auto",  # "zn", "en", "yue", "ja", "ko", "nospeech"
        use_itn=True,
        batch_size_s=60,
        merge_vad=True,  #
        merge_length_s=15,
    )

    result = rich_transcription_postprocess(res[0]["text"])

    # 清理显存缓存
    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()

    # 返回识别结果
    return {"asr_result": result}


@router.post("/audio_to_text")
def audio_to_text(audio: UploadFile):
    localdir = f"statics/upload/{datetime.now().strftime('%Y-%m-%d')}/{uuid.uuid4()}"
    os.makedirs(localdir, exist_ok=True)
    localfile = f"{localdir}/{audio.filename}"

    with open(localfile, "wb") as f:
        f.write(audio.file.read())

    res = model.generate(
        input=localfile,
        cache={},
        language="auto",  # "zn", "en", "yue", "ja", "ko", "nospeech"
        use_itn=True,
        batch_size_s=60,
        merge_vad=True,  #
        merge_length_s=15,
    )

    result = rich_transcription_postprocess(res[0]["text"])

    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()

    return result
