from fastapi import APIRouter, Request, HTTPException, Response
from fastapi.responses import FileResponse

import os
import base64
import re
import json
import torch
import numpy as np
import uuid
import soundfile

from configs import config

from datetime import datetime

from melotts.api import TTS
model = TTS(language="ZH", device="auto")
speaker_ids = model.hps.data.spk2id

router = APIRouter(
    prefix='/tts/melotts',
    tags = ['语音生成']
)

def tts_to_file(input):
    localdir = f"audio/{datetime.now().strftime('%Y-%m-%d')}"
    os.makedirs(f"{config.setting.statics.path}/{localdir}", exist_ok=True)
    localfile = f"{localdir}/{uuid.uuid4()}.wav"
    
    model.tts_to_file(input, speaker_ids['ZH'], f"{config.setting.statics.path}/{localfile}", speed=0.7)

    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()
    
    return localfile


"""
文本数据转语音
"""
@router.post("/v1")
def generate_audio_post(request: dict):
    # 将文本数据转换成语音，保存到本地文件
    localfile = tts_to_file(request["text"])

    # 获取本地文件的字节数据信息
    with open(f"{config.setting.statics.path}/{localfile}", 'rb') as audio_file:
        """
        读取文件内容到字节字符串
        """
        audio_bytes = audio_file.read()
    
    # 将字节数据做Base64编码，编码成字符串，并返回
    return {"tts_result": base64.b64encode(audio_bytes).decode('utf-8')}


@router.post("/audio/speech")
def audio_speech(request: dict):
    # return FileResponse(tts_to_file(request["input"]), media_type="audio/wav")
    return {
                "data": {
                    "url": f"{config.setting.statics.urls}/{tts_to_file(request['input'])}"
                }
           }


@router.post("/generate_audio_url")
def generate_audio_url(request: dict):
    return f"{config.setting.statics.urls}/{tts_to_file(request['input'])}"


@router.post("/generate_audio_file")
def generate_audio_file(request: dict):
    return FileResponse(f"{config.setting.statics.path}/{tts_to_file(request['input'])}", media_type="audio/wav")
