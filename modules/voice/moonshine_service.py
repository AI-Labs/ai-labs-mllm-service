from transformers import AutoModelForSpeechSeq2Seq, AutoConfig, PreTrainedTokenizerFast

import torchaudio
import sys

audio, sr = torchaudio.load("/root/AI-Labs/ai-labs-service/statics/audio/test.wav")
if sr != 16000:
  audio = torchaudio.functional.resample(audio, sr, 16000)

MODEL_PATH = "/root/AI-Labs/ai-labs-service/models/UsefulSensors/moonshine-base"
model = AutoModelForSpeechSeq2Seq.from_pretrained(MODEL_PATH, trust_remote_code=True)
tokenizer = PreTrainedTokenizerFast.from_pretrained(MODEL_PATH)

tokens = model(audio)
print(tokenizer.decode(tokens[0], skip_special_tokens=True))