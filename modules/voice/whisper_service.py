from fastapi import APIRouter, Request, HTTPException, Response, UploadFile

import whisper
import torch

import os
import uuid
from datetime import datetime

from configs import config


router = APIRouter(
    prefix='/asr/whisper',
    tags = ['语音识别']
)

model = whisper.load_model(config.service.whisper.name, download_root=config.service.whisper.model_path)


@router.post("/audio_to_text")
def audio_to_text(audio: UploadFile):
    localdir = f"statics/upload/{datetime.now().strftime('%Y-%m-%d')}/{uuid.uuid4()}"
    os.makedirs(localdir, exist_ok=True)
    localfile = f"{localdir}/{audio.filename}"

    with open(localfile, "wb") as f:
        f.write(audio.file.read())

    result = model.transcribe(localfile)
    result = result["text"]
    
    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()

    return result