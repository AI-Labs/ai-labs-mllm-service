JPype1
ipywidgets
jupyterlab
jupyterlab-language-pack-zh-CN
jupyter-server-proxy
pyNetLogo
SALib

numpy
pandas
matplotlib
scipy
seaborn
selenium==4.23.1
python-multipart

torch
torchvision
torchaudio
# diffusers
# huggingface_hub
modelscope

openai>=1.34.0
einops>=0.6.1
omegaconf
sse-starlette>=2.1.0

# glm-4-9b-chat

pydantic>=2.7.1
timm>=0.9.16
# tiktoken>=0.7.0
# accelerate>=0.30.1
sentence_transformers>=2.7.0

cpm_kernels
mdtex2html
# sentencepiece
loguru
icetk
# Pillow
protobuf

# chattts

transformers
vector_quantize_pytorch==1.12.17
vocos
soundfile

# melotts

# librosa==0.9.1
cn2an==0.5.22
pypinyin==0.50.0
jieba==0.42.1
g2p_en==2.1.0
txtsplit==1.0.0
cached_path==1.6.2
num2words==0.5.12
anyascii==0.3.2
jamo==0.4.1

# internvl

peft==0.12.0

# qwen2vl

qwen-vl-utils

# whisper

whisper==1.1.10
openai-whisper
more-itertools==10.3.0
triton==3.0.0

# anydoor

albumentations==1.3.0
fvcore==0.1.5.post20221221
open_clip_torch
# opencv_contrib_python==4.6.0.66
# opencv_python==4.6.0.66
# opencv_python_headless==4.6.0.66
# Pillow==9.4.0
pytorch_lightning==1.5.0
# safetensors==0.4.3
# setuptools==66.0.0
share==1.0.4
submitit==1.5.1
torchmetrics==0.6.0

# catvton

tqdm==4.66.4
xformers==0.0.23

# v-express

# diffusers==0.27.2
insightface==0.7.3
imageio-ffmpeg==0.4.9
av==12.1.0

# hallo

audio-separator==0.17.2
# av==12.1.0
bitsandbytes==0.42
decord==0.6.0
# einops==0.8.0
# insightface==0.7.3
librosa==0.10.2.post1
mediapipe[vision]==0.10.14
mlflow==2.13.1
moviepy==1.0.3
# omegaconf==2.3.0
onnx2torch==1.5.0
onnx==1.16.1
onnxruntime-gpu
opencv-contrib-python==4.9.0.80
opencv-python-headless==4.9.0.80
opencv-python==4.9.0.80
pillow==10.3.0
# setuptools==70.0.0
isort==5.13.2
pylint==3.2.2
pre-commit==3.7.1

# git+https://github.com/facebookresearch/detectron2.git
# git+https://github.com/facebookresearch/detectron2@main#subdirectory=projects/DensePose

# pip install -e setups/transformers
# pip install -e setups/detectron2
# pip install -e setups/detectron2/projects/DensePose
# pip uninstall deepspeed xformers
