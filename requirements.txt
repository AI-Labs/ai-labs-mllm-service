JPype1
ipywidgets
jupyterlab
jupyterlab-language-pack-zh-CN
jupyter-server-proxy
pyNetLogo
SALib

numpy==1.26.4
pandas
matplotlib==3.9.1
scipy==1.13.1
seaborn
selenium==4.23.1
python-multipart==0.0.9

torch==2.1.2
torchvision==0.16.2
torchaudio
# transformers==4.44.2 # 4.39.3
huggingface_hub==0.23.5
modelscope==1.19.2
vllm

openai>=1.34.0
einops>=0.6.1
omegaconf
sse-starlette>=2.1.0

# glm-4-9b-chat

pydantic>=2.7.1
timm>=0.9.16
# tiktoken>=0.7.0
accelerate==0.31.0
sentence_transformers>=2.7.0

cpm_kernels
mdtex2html
# sentencepiece
loguru
icetk
# Pillow
protobuf

# chattts

vector_quantize_pytorch==1.12.17
vocos
soundfile

# melotts

# librosa==0.9.1
cn2an==0.5.22
pypinyin==0.50.0
jieba==0.42.1
g2p_en==2.1.0
txtsplit==1.0.0
cached_path==1.6.3
num2words==0.5.12
anyascii==0.3.2
jamo==0.4.1

# internvl

peft==0.12.0

# qwen2vl

-e setups/transformers
qwen-vl-utils

# whisper

whisper==1.1.10
openai-whisper
more-itertools==10.3.0
triton

# funasr

torch_complex==0.4.4
kaldiio>=2.17.0
hydra-core>=1.3.2


# anydoor

albumentations==1.3.0
fvcore==0.1.5.post20221221
open_clip_torch
# opencv_contrib_python==4.6.0.66
# opencv_python==4.6.0.66
# opencv_python_headless==4.6.0.66
pytorch_lightning==1.5.0
# safetensors==0.4.3
# setuptools==66.0.0
share==1.0.4
submitit==1.5.1
torchmetrics==0.6.0

############################################################
# catvton
############################################################

diffusers==0.29.2
Ninja==1.11.1.1
opencv_contrib_python==4.10.0.84
opencv_python==4.10.0.84
opencv_python_headless==4.10.0.84
pillow==10.3.0
PyYAML==6.0.1
scikit-image==0.24.0
# setuptools==51.0.0
tqdm==4.66.4
xformers==0.0.23.post1

############################################################
# hallo
############################################################

audio-separator==0.17.2
av==12.1.0
bitsandbytes==0.42
decord==0.6.0
# einops==0.8.0
insightface==0.7.3
imageio-ffmpeg==0.4.9
isort==5.13.2
librosa==0.10.2.post1
mediapipe[vision]==0.10.14
mlflow==2.13.1
moviepy==1.0.3
# omegaconf==2.3.0
onnx2torch==1.5.0
onnx==1.16.1
onnxruntime-gpu
# opencv-contrib-python==4.9.0.80
# opencv-python-headless==4.9.0.80
# opencv-python==4.9.0.80
# pillow==10.3.0
pre-commit==3.7.1
pylint==3.2.2
# setuptools==70.0.0  # ImportError: cannot import name 'packaging' from 'pkg_resources'
setuptools==69.5.1

icecream==2.1.3

# # wav2lip

# basicsr==1.4.2
# batch-face==1.4.0
# dlib==19.24.2
# facexlib==0.3.0
# gdown==4.7.1
# gfpgan==1.3.8
# importlib-metadata==6.8.0
# # librosa==0.10.1
# # moviepy==1.0.3

# # musetalk

# tensorflow==2.12.0
# tensorboard==2.12.0
# spaces==0.30.2
# requests
# ffmpeg-python

# mmpose
# mmcv>=2.0.0rc4, <2.2.0
# mmdet

# git+https://github.com/facebookresearch/detectron2.git
# git+https://github.com/facebookresearch/detectron2@main#subdirectory=projects/DensePose

# pip install -e setups/transformers
# pip install -e setups/detectron2
# pip install -e setups/detectron2/projects/DensePose
