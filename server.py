import sys
import uvicorn
import torch

from contextlib import asynccontextmanager
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from configs import config

@asynccontextmanager
async def lifespan(app: FastAPI):
    yield
    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.ipc_collect()

if __name__ == "__main__":

    app = FastAPI(lifespan=lifespan)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.mount("/statics", StaticFiles(directory="statics"), name="statics")

    from modules.common import common
    app.include_router(common.router)

    # from modules.common import tools
    # app.include_router(tools.router)

    # from modules.common import guard
    # app.include_router(guard.router)

    if config.service.chatvllm.enable == True:
        from modules.chat import chatvllm_service
        app.include_router(chatvllm_service.router)
    if config.service.chatglm.enable == True:
        from modules.chat import chatglm_service
        app.include_router(chatglm_service.router)
    if config.service.internlm.enable == True:
        from modules.chat import internlm_service
        app.include_router(internlm_service.router)
    if config.service.yi.enable == True:
        from modules.chat import yi_service
        app.include_router(yi_service.router)

    if config.service.chattts.enable == True:
        sys.path.append("modules/voice")
        from modules.voice import chattts_service
        app.include_router(chattts_service.router)
    if config.service.melotts.enable == True:
        sys.path.append("modules/voice")
        from modules.voice import melotts_service
        app.include_router(melotts_service.router)

    if config.service.funasr.enable == True:
        sys.path.append("modules/voice/FunASR")
        from modules.voice import funasr_service
        app.include_router(funasr_service.router)
    if config.service.whisper.enable == True:
        from modules.voice import whisper_service
        app.include_router(whisper_service.router)

    if config.service.stable_diffusion_3.enable == True:
        from modules.image import stable_diffusion_3_service
        app.include_router(stable_diffusion_3_service.router)
    if config.service.stable_diffusion_xl.enable == True:
        from modules.image import stable_diffusion_xl_service
        app.include_router(stable_diffusion_xl_service.router)

    if config.service.internvl.enable == True:
        from modules.vision_language import internvl_service
        app.include_router(internvl_service.router)
    if config.service.qwen2vl.enable == True:
        from modules.vision_language import qwen2vl_service
        app.include_router(qwen2vl_service.router)

    if config.service.anydoor.enable == True:
        sys.path.append("modules/try_on/anydoor")
        sys.path.append("modules/try_on/anydoor/dinov2")
        sys.path.append("modules/try_on/catvton")
        from modules.try_on import anydoor_service
        app.include_router(anydoor_service.router)
    if config.service.catvton.enable == True:
        sys.path.append("modules/try_on/catvton")
        from modules.try_on import catvton_service
        app.include_router(catvton_service.router)
    if config.service.leffa.enable == True:
        sys.path.append("modules/try_on/Leffa")
        from modules.try_on import leffa_service
        app.include_router(leffa_service.router)


    if config.service.hallo.enable == True:
        if config.service.hallo.version == "hallo1":
            sys.path.append("modules/digital_human/hallo1")
            from modules.digital_human import hallo1_service
            app.include_router(hallo1_service.router)
        if config.service.hallo.version == "hallo2":
            sys.path.append("modules/digital_human/hallo2")
            from modules.digital_human import hallo2_service
            app.include_router(hallo2_service.router)
    if config.service.v_express.enable == True:
        sys.path.append("modules/digital_human")
        from modules.digital_human import v_express_service
        app.include_router(v_express_service.router)
    if config.service.easy_wav2lip.enable == True:
        sys.path.append("modules/digital_human")
        from modules.digital_human import easy_wav2lip_service
        app.include_router(easy_wav2lip_service.router)

    uvicorn.run(app, host=config.server.host, port=config.server.port, workers=1)
