#!/bin/bash

export HF_ENDPOINT=https://hf-mirror.com

rm -rf nohup.out

nohup python server.py &
